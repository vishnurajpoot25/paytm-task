import React,{useState,useRef} from 'react'
import './App.css'
import classNames from "classnames";

function RichTextEditor() {

    const [fontItem,setFontItem] = useState({
        bold:false,
        italic:false,
        underline:false,
        fontSize:11,
        colorHex:'#000000'
    }) 
    const editorRef = useRef()
 
    const fontHandler  = (e)=>{ 
        let txt = window.getSelection(); 
        let start =  txt.focusOffset
        let end =  txt.anchorOffset  
         
        if( start != end){ 
            let selectedText = editorRef.current.innerText.substring(start, end); 
            console.log(selectedText)
            let completeText = editorRef.current.innerText;
            let rx = RegExp(completeText.substr(start,end - start), "g");   
            let finalText = completeText.replace(rx, function(m, offset, input) {  
                var result = offset === start ?  `<span class="${e.target.id}" >`+selectedText+'</span>':m;  
                return result;
            }) 
            editorRef.current.innerHTML = finalText
        } 
        setFontItem(prevState=>({
            ...fontItem,  [e.target.id]: !prevState[e.target.id]
        }))    
         let allClass=classNames('fontclass',{
            bold: fontItem.bold,
            italic: fontItem.italic,
            [e.target.id]: [e.target.id]
          })
         editorRef.current.innerHTML =  editorRef.current.innerHTML+`<span class="${allClass}" >&nbsp;</span>`; 
    } 

    const inputHandler = (e)=>{  
        let allClass=classNames('ipClass',{
            bold: fontItem.bold,
            italic: fontItem.italic
          }) 
          setFontItem(prevState=>({
            ...fontItem,  [e.target.id]: e.target.value
        }))   
        let fontSize = (e.target.id =='fontSize') ? e.target.value : fontItem.fontSize
        let colorHex = (e.target.id =='colorHex') ? e.target.value : fontItem.colorHex 
        console.log(e.target.value )
        editorRef.current.innerHTML =  editorRef.current.innerHTML+`<span   style="font-size:${fontSize}px;color:${colorHex}" class="${allClass}" >&nbsp;</span>`; 
    } 
    return (
        <div className="mainDiv"><br/>
            
            <div>
                <button onClick={fontHandler} id="bold" className={(fontItem.bold === true ? "selected" : "")}>Bold</button>&nbsp;
                <button onClick={fontHandler} id="italic" className={(fontItem.italic === true ? "selected" : "")}>Italic</button>&nbsp;
                <button onClick={fontHandler} id="underline" className={(fontItem.underline === true ? "selected" : "")}>Underline</button>&nbsp;
                <input type="text"  className="ip" name="fontSize" id="fontSize" placeholder="Font Size (px)" onBlur={inputHandler}  />
                <input type="text" className="ip" name="colorHex" id="colorHex" placeholder="Color Hex Code" onBlur={inputHandler} />
                <button className="fex">List</button>
            </div>
            <div contentEditable="true" className="editor" ref={editorRef}>

               <span> Editor...</span> 
            </div>
        </div>
    )
}

export default RichTextEditor
